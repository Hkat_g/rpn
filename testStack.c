#include <stdio.h>
#include "stack.h"
#include "testCommon.h"

void testInitStack() {
    stack myStack;
    testStart("InitStack");
    myStack.top = 0; // 適当な値にセット
    initStack(&myStack, N);
    assertEqualsInt(myStack.top, N); // sp が最終値より後ろになっているか
}

void testTopValue(void) {
	stack myStack;
	testStart("topValue");
	initStack(&myStack, N);
	/* stack が空なら NaN を返す */
	assertEqualsInt(isnan(topValue(&myStack)), 1);
	/* stack が空でなければスタックのさしている数字を返す */
	myStack.top = N - 1;
	myStack.data[N - 1] = 5.0;
	assertEqualsInt(isnan(topValue(&myStack)), 0); // not NaN
	assertEqualsDouble(topValue(&myStack), 5.0);
	myStack.top = N - 2;
	myStack.data[N - 2] = 3.0;
	assertEqualsInt(isnan(topValue(&myStack)), 0); // not NaN
	assertEqualsDouble(topValue(&myStack), 3.0);
}

void testPush() {
	stack myStack;
	testStart("push");
	initStack(&myStack, N);
	push(&myStack, 5.0);
	assertEqualsDouble(topValue(&myStack), 5.0);
	assertEqualsInt(myStack.top, N-1);
	push(&myStack, 3.0);
	assertEqualsDouble(topValue(&myStack), 3.0);
	assertEqualsInt(myStack.top, N-2);
}

void testPull() {
	stack myStack;
	testStart("pull");
	initStack(&myStack, N);
	push(&myStack, 5.0);
	push(&myStack, 3.0);
	assertEqualsDouble(pull(&myStack), 3.0);
	assertEqualsInt(myStack.top, N-1);
	assertEqualsDouble(pull(&myStack), 5.0);
	assertEqualsInt(myStack.top, N);
	assertEqualsInt(pull(&myStack), sqrt(-1.0));
	assertEqualsInt(myStack.top, N);
}

void testPrintStack() {
	stack myStack;
	testStart("PrintStack");
	initStack(&myStack, N);
	push(&myStack, 5.0);
	push(&myStack, 3.0);
	PrintStack(&myStack);
}

void testAddStack() {
	stack myStack;
	testStart("AddStack");
	initStack(&myStack, N);
	push(&myStack, 5.0);
	push(&myStack, 3.0);
	AddStack(&myStack);
	assertEqualsInt(topValue(&myStack), 8);
	assertEqualsInt(myStack.top, N - 1);
	AddStack(&myStack);
	assertEqualsInt(isnan(topValue(&myStack)), 1);
	assertEqualsInt(myStack.top, N - 1);
}

void testMulStack() {
	stack myStack;
	testStart("MulStack");
	initStack(&myStack, N);
	push(&myStack, 5.0);
	push(&myStack, 3.0);
	MulStack(&myStack);
	assertEqualsInt(topValue(&myStack), 15.0);
	assertEqualsInt(myStack.top, N - 1);
	MulStack(&myStack);
	assertEqualsInt(isnan(topValue(&myStack)), 1);
	assertEqualsInt(myStack.top, N - 1);
}

void testSubStack() {
	stack myStack;
	testStart("SubStack");
	initStack(&myStack, N);
	push(&myStack, 5.0);
	push(&myStack, 3.0);
	SubStack(&myStack);
	assertEqualsInt(topValue(&myStack), 2.0);
	assertEqualsInt(myStack.top, N - 1);
	SubStack(&myStack);
	assertEqualsInt(isnan(topValue(&myStack)), 1);
	assertEqualsInt(myStack.top, N - 1);
}

void testDivStack() {
	stack myStack;
	testStart("DivStack");
	initStack(&myStack, N);
	push(&myStack, 12.0);
	push(&myStack, 3.0);
	DivStack(&myStack);
	assertEqualsInt(topValue(&myStack), 4.0);
	assertEqualsInt(myStack.top, N - 1);
	DivStack(&myStack);
	assertEqualsInt(isnan(topValue(&myStack)), 1);
	assertEqualsInt(myStack.top, N - 1);
}

int main() {
    testInitStack();
	testTopValue();
	testPush();
	testPull();
	testPrintStack();
	testAddStack();
	testMulStack();
	testSubStack();
	testDivStack();
    testErrorCheck();
    return 0;
}
