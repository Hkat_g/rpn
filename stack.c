#include "stack.h"
#include <math.h>
#include <stdio.h>

void initStack(stack *sp, int n) {
	sp->top = n;
}

double topValue(stack *sp) {
	return sp->top == N ? sqrt(-1.0) : sp->data[sp->top];
}

void push(stack *sp, double x) {
	sp->data[--sp->top] = x;
}

double pull(stack *sp) {
	return (sp->top == N) ? sqrt(-1.0) :  sp->data[sp->top++];
}

void PrintStack(stack *sp) {
	int i = 0;
	for (i = N; sp->top < i;i--) {
		printf("%10.10lf\n", sp->data[i-1]);
	}
}

void AddStack(stack *sp) {
	push(sp, pull(sp) + pull(sp));
}

void MulStack(stack *sp) {
	push(sp, pull(sp) * pull(sp));
}

void SubStack(stack *sp) {
	double temp = pull(sp);
	push(sp, pull(sp) - temp);
}

void DivStack(stack *sp) {
	double temp = pull(sp);
	push(sp, pull(sp) / temp);
}

void SineStack(stack *sp) {
	push(sp, sin(pull(sp)*M_PI/180));
}

void CosStack(stack *sp) {
	push(sp, cos(pull(sp)*M_PI / 180));
}

void TanStack(stack *sp) {
	push(sp, tan(pull(sp)*M_PI / 180));
}

void SqrtStack(stack *sp) {
	push(sp, sqrt(pull(sp)));
}

void PowStack(stack *sp) {
	double temp = pull(sp);
	push(sp, pow(pull(sp), temp));
}

void ExpStack(stack *sp) {
	push(sp, exp(pull(sp)));
}

void LogStack(stack *sp) {
	push(sp, log(pull(sp)));
}

void Log10Stack(stack *sp) {
	push(sp, log10(pull(sp)));
}

void AbsStack(stack *sp) {
	push(sp, fabs(pull(sp)));
}

void CurtStack(stack *sp) {
	double temp = pull(sp);
	push(sp, pow(pull(sp), 1.0/temp));
}

void Help() {
	printf("e ... End rpn.\n");
	printf("p ... Pop and show the newest value from the stack.\n");
	printf("sin ... Sine function.\n");
	printf("cos ... Cosine function.\n");
	printf("tan ... Tangent function.\n");
	printf("sqrt ... Square root function.\n");
	printf("curt ... Cube root function.\n");
	printf("pow ... Power function.\n");
	printf("exp ... Exponential function.\n");
	printf("log ... Natural logarithm function.\n");
	printf("log10 ... Common logarithm function.\n");
	printf("abs ... Absolute value.\n");
}