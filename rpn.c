#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "stack.h"

#define LEN 200
char line[LEN] = {};

void checkInclchar(char *st, int *flag) {
	int i=0;

	for (i = 1; i < LEN; i++) {
		if (st[0] == '+' || st[0] == '-' || (st[0] >= '0'/*0x30*/ && st[0] <= '9'/*0x39*/) || st[0] == 0x00) { //top sign
			//printf("Exist sign.\n");
			if ((st[i] >= '0'/*0x30*/ && st[i] <= '9'/*0x39*/)/*0�`9*/ || st[i] == '\n'/*0x0a*//*n*/ || st[i] == '.' /*0x2e*/ || st[i] == 0x00) { /*printf("Exist arrawed com1.\n");*/ }	//White list
			else {
				/*printf("Exist invalid com1.\n");*/
				*flag = 1;
			}
		}
		else {
			if ((st[i-1] >= '0'/*0x30*/ && st[i-1] <= '9'/*0x39*/)/*0�`9*/ || st[i-1] == '+'/*0x2b*//*+*/ || st[i-1] == '-'/*0x2d*//*-*/ || st[i-1] == '\n'/*0x0a*//*n*/ || st[i-1] == '.' || st[i-1] == 0x00) { /*printf("Exist arrawed com2.\n");*/ }
			else {
				/*printf("Exist invalid com2.\n");*/
				*flag = 1;
			}
		}
		
		//printf("%x\n", st[i]);
	}
}

int main() {
	double temp = 0;
	int flagInclchar = 0;
	stack myStack;
	initStack(&myStack, N);

	while (1) {
		//�����܂ݔ��菉����
		memset(line, '\0', LEN);
		flagInclchar = 0;

		printf("Type a number or command (Display command list with [help] command.)\n =>");
		fgets(line, LEN, stdin);
		checkInclchar(line, &flagInclchar);

		if (strcmp(line, "+\n") == 0) {
			if (myStack.top == N || myStack.top == N-1) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				AddStack(&myStack);
			}
		}
		else if (strcmp(line, "-\n") == 0) {
			if (myStack.top == N || myStack.top == N-1) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				SubStack(&myStack);
			}
		}
		else if (strcmp(line, "*\n") == 0) {
			if (myStack.top == N || myStack.top == N-1) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				MulStack(&myStack);
			}
		}
		else if (strcmp(line, "/\n") == 0) {
			if (myStack.top == N || myStack.top == N-1) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				DivStack(&myStack);
			}
		}
		else if (strcmp(line, "sin\n") == 0) {
			if (myStack.top == N) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				SineStack(&myStack);
			}
		}
		else if (strcmp(line, "cos\n") == 0) {
			if (myStack.top == N) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				CosStack(&myStack);
			}
		}
		else if (strcmp(line, "tan\n") == 0) {
			if (myStack.top == N) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				TanStack(&myStack);
			}
		}
		else if (strcmp(line, "sqrt\n") == 0) {
			if (myStack.top == N) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				SqrtStack(&myStack);
			}
		}
		else if (strcmp(line, "pow\n") == 0) {
			if (myStack.top == N || myStack.top == N - 1) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				PowStack(&myStack);
			}
		}
		else if (strcmp(line, "exp\n") == 0) {
			if (myStack.top == N) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				ExpStack(&myStack);
			}
		}
		else if (strcmp(line, "log\n") == 0) {
			if (myStack.top == N) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				LogStack(&myStack);
			}
		}
		else if (strcmp(line, "log10\n") == 0) {
			if (myStack.top == N) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				Log10Stack(&myStack);
			}
		}
		else if (strcmp(line, "abs\n") == 0) {
			if (myStack.top == N) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				AbsStack(&myStack);
			}
		}
		else if (strcmp(line, "curt\n") == 0) {
			if (myStack.top == N || myStack.top == N - 1) {
				printf("Can't calc because lack of values in stack!!!\n");
			}
			else {
				CurtStack(&myStack);
			}
		}
		else if (strcmp(line, "help\n") == 0) {
			Help();
		}
		else if (strcmp(line, "e\n") == 0) {
			break;
		}
		else if (strcmp(line, "p\n") == 0) {
			temp = pull(&myStack);
			if (isnan(temp) == 0) {
				printf("Pop value -> %10.10lf\n", temp);
			}
			else {
				printf("Can't pop because no value in stack !!!\n");
			}
			
		}
		else {
			if (flagInclchar == 1) {
				printf("Unknown command !!!\n");
				
			}
			else {
				push(&myStack, atof(line));
			}
		}

		if (isnan(myStack.data[myStack.top]) == 1 || myStack.top == N) {
			printf("Stack is empty !!!\n");
		}
		else {
			printf("Stack value is\n");
			PrintStack(&myStack);
		}
		printf("\n");
	}
    
    return 0;
}

