#define N 10000

typedef struct{
	double data[N];
	int top;
} stack;

void initStack(stack *sp, int n);
double topValue(stack *sp);
void push(stack *sp, double x);
double pull(stack *sp);
void PrintStack(stack *sp);
void AddStack(stack *sp);
void MulStack(stack *sp);
void SubStack(stack *sp);
void DivStack(stack *sp);
void SineStack(stack *sp);
void CosStack(stack *sp);
void TanStack(stack *sp);
void SqrtStack(stack *sp);
void PowStack(stack *sp);
void ExpStack(stack *sp);
void LogStack(stack *sp);
void Log10Stack(stack *sp);
void AbsStack(stack *sp);
void CurtStack(stack *sp);
void Help();